<?php

namespace CodingPaws\FindBy;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class NamedBuilder extends Builder
{
    public function findBy(...$params): self
    {
        return $this->where(fn (self $q) => $q->performWheres($params));
    }

    public function findByNot(...$params)
    {
        return $this->where(fn (self $q) => $q->performWheres($params, not: true));
    }

    public function orFindBy(...$params)
    {
        return $this->orWhere(fn (self $q) => $q->performWheres($params));
    }

    public function orFindByNot(...$params)
    {
        return $this->orWhere(fn (self $q) => $q->performWheres($params, not: true));
    }

    public function performWheres(array $params, bool $not = false): self
    {
        $query = $this;

        foreach ($params as $key => $value) {
            $query = $query->performWhere($not, $key, $value);
        }

        return $query;
    }

    public function performWhere(bool $not, string $key, mixed $value): self
    {
        if (is_array($value) || $value instanceof Collection) {
            $method = $not ? 'whereNotIn' : 'whereIn';

            return $this->{$method}($key, $value);
        }

        return $this->where($key, $not ? '!=' : '=', $value);
    }
}
