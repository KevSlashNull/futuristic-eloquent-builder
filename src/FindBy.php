<?php

namespace CodingPaws\FindBy;

/**
 * @method static NamedBuilder findBy(...$args)
 * @method static NamedBuilder findByNot(...$args)
 * @method static NamedBuilder orFindBy(...$args)
 * @method static NamedBuilder orFindByNot(...$args)
 */
trait FindBy
{
    public function newEloquentBuilder($query)
    {
        return new NamedBuilder($query);
    }
}
