<?php

namespace CodingPaws\FindBy\Tests;


class OrFindByNotTest extends TestCase
{
    public function testSingleWhere()
    {
        $query = Example::orFindByNot(name: 'test');

        $this->assertSql("(`name` != ?)", $query);
        $this->assertBindings(['test'], $query);
    }

    public function testDoubleWhere()
    {
        $query = Example::orFindByNot(user_id: 5)->orFindByNot(is_admin: true);

        $this->assertSql("(`user_id` != ?) or (`is_admin` != ?)", $query);
        $this->assertBindings([5, true], $query);
    }

    public function testMultipleParameters()
    {
        $query = Example::orFindByNot(user_id: 5, is_admin: true, ownable: false, errors: 0)
            ->orFindByNot(type: 'Customer', owner: null);

        $this->assertSql("(`user_id` != ? and `is_admin` != ? and `ownable` != ? and `errors` != ?) or (`type` != ? and `owner` is not null)", $query);
        $this->assertBindings([5, true, false, 0, 'Customer'], $query);
    }

    public function testMixedUsage()
    {
        $query = Example::whereUserId(5)->orFindByNot(owner: null, is_admin: false)
            ->where('type', '!=', 'Customer');

        $this->assertSql("`user_id` = ? or (`owner` is not null and `is_admin` != ?) and `type` != ?", $query);
        $this->assertBindings([5, false, 'Customer'], $query);
    }

    public function testWithArrayParameters()
    {
        $query = Example::orFindByNot(name: ['doggo', 'kitty'], owner: null);

        $this->assertSql("(`name` not in (?, ?) and `owner` is not null)", $query);
        $this->assertBindings(['doggo', 'kitty'], $query);
    }
}
