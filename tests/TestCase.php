<?php

namespace CodingPaws\FindBy\Tests;

use CodingPaws\FindBy\NamedBuilder;
use Orchestra\Testbench\TestCase as TestbenchTestCase;

class TestCase extends TestbenchTestCase
{
  protected const BASE = 'select * from `examples` where ';

  protected function assertSql(string $expected, NamedBuilder $actual)
  {
    $this->assertEquals(self::BASE . $expected, $actual->toSql(), 'SQL WHERE part is wrong');
  }

  protected function assertBindings(array $expected, NamedBuilder $actual)
  {
    $this->assertEquals($expected, $actual->getBindings(), 'Bindings are not equal');
    $this->assertEquals(json_encode($expected), json_encode($actual->getBindings()), 'Bindings must be exactly (!) equal.');
  }
}
