<?php

namespace CodingPaws\FindBy\Tests;


class FindByTest extends TestCase
{
    const BASE = 'select * from `examples` where ';

    public function testSingleWhere()
    {
        $query = Example::findBy(name: 'test');

        $this->assertSql("(`name` = ?)", $query);
        $this->assertBindings(['test'], $query);
    }

    public function testDoubleWhere()
    {
        $query = Example::findBy(user_id: 5)->findBy(is_admin: true);

        $this->assertSql("(`user_id` = ?) and (`is_admin` = ?)", $query);
        $this->assertBindings([5, true], $query);
    }

    public function testMultipleParameters()
    {
        $query = Example::findBy(user_id: 5, is_admin: true, ownable: false, errors: 0)
            ->findBy(type: 'Customer', owner: null);

        $this->assertSql("(`user_id` = ? and `is_admin` = ? and `ownable` = ? and `errors` = ?) and (`type` = ? and `owner` is null)", $query);
        $this->assertBindings([5, true, false, 0, 'Customer'], $query);
    }

    public function testMixedUsage()
    {
        $query = Example::whereUserId(5)->findBy(owner: null, is_admin: false)
            ->where('type', '!=', 'Customer');

        $this->assertSql("`user_id` = ? and (`owner` is null and `is_admin` = ?) and `type` != ?", $query);
        $this->assertBindings([5, false, 'Customer'], $query);
    }

    public function testWithArrayParameters()
    {
        $query = Example::findBy(name: ['doggo', 'kitty'], owner: null);

        $this->assertSql("(`name` in (?, ?) and `owner` is null)", $query);
        $this->assertBindings(['doggo', 'kitty'], $query);
    }
}
