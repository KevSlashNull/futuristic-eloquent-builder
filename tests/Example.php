<?php

namespace CodingPaws\FindBy\Tests;

use Illuminate\Database\Eloquent\Model;
use CodingPaws\FindBy\FindBy;

class Example extends Model
{
    use FindBy;
}
